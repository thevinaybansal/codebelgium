<?php

return[

    'assets-theme'                  =>  '/assets/web/theme/',
    'assets-theme-css'              =>  '/assets/web/theme/css/',
    'assets-theme-js'               =>  '/assets/web/theme/js/',
    'assets-theme-images'           =>  '/assets/web/theme/images/',
    'assets-theme-fonts'            =>  '/assets/web/theme/fonts/',

    'assets-admin'                  =>  '/assets/admin/',
    'assets-admin-css'              =>  '/assets/admin/css/',
    'assets-admin-js'               =>  '/assets/admin/js/',
    'assets-admin-media'            =>  '/assets/admin/media/',
    'assets-admin-vendors'          =>  '/assets/admin/vendors/',
    'assets-admin-widgets'          =>  '/assets/admin/widgets/',

    'assets-site'                   =>  '/assets/site/',
    'logo'                          =>  '/assets/site/logo/',
    'favicon'                       =>  '/assets/site/favicon/',
    'post-media'                    =>  '/assets/site/postsmedia/',
    'users'                         =>  '/assets/site/users/',
    


];

<?php

namespace Deployer;


desc('Generating Sitemap');
task('sitemap', function () {
    run('{{bin/php}} {{release_path}}/artisan sitemap:generate');
});

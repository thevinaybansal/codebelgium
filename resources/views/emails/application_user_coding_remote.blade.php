Hello {{ $application->name }},<br>
<br>
Greetings of the day! <br>
<br>
Thanks for applying at CodeBelgium for Remote Coding Program. We are currently open for admissions.<br>
<br>
You need to go through a small interview process to continue with your application.<br>
<br>
For the interview process, schedule a free call with one of our instructors, using the given link below as per your convenient time<br>
<br>
https://calendly.com/thevinaybansal/codebelgium-interview<br>
<br>
Contact the instructor at the selected date and time on Skype at ID : "vinay.bansal25".<br>
<br>
Schedule the meeting earliest to proceed with your application, and reserve your seat for the batch.<br>
<br>
If any questions, feel free to contact us.<br>
<br>
Thank You<br>
Amruta Rathod<br>
CodeBelgium Student Advisor
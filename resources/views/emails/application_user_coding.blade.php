Hello {{ $application->name }},<br>
<br>
Greetings of the day! <br>
<br>
<p>As you must be aware of COVID-19 Situation currently. We are not taking any applications for coding bootcamp on-campus. Currently, we have admissions open for our remote coding program. In our remote coding program, one of our instructors will guide you online and mentor you using video conferencing platform to complete your program.</p>
<br>
<p>You need to go through a small interview process to continue with your application.</p>
<br>
<p>For the interview process, schedule a free call with one of our instructors, using the given link below as per your convenient time</p>
<br>
https://calendly.com/thevinaybansal/codebelgium-interview<br>
<br>
Contact the instructor at the selected date and time on Skype at ID : "vinay.bansal25".<br>
<br>
Schedule the meeting earliest to proceed with your application, and reserve your seat for the batch.<br>
<br>
If any questions, feel free to contact us.<br>
<br>
Thank You<br>
Amruta Rathod<br>
CodeBelgium Student Advisor
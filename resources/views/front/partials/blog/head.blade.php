<head>
  
  {{-- Google Tag Manager --}}
  @include('googletagmanager::head')
  {{-- End Google Tag Manager --}}

  <meta charset="utf-8"> <title>@yield('title')</title>
  
  {{-- Favicon --}}
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('directory.favicon') . 'favicon.ico') }}">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  
  <meta content="@yield('keywords')" name="keywords">
  <meta content="@yield('description')" name="description">

  @stack('head')

  {{-- Script Compiled --}}
  <link href="{{ mix('css/app.css', 'assets/web') }}" rel="stylesheet">
  
  @stack('css')
  @stack('stylesheet')  
</head>
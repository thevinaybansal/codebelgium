<footer class="animate" data-animate="fadeInDown" data-duration="1s" data-delay="0s">
	<div class="copyrights container">
    	<a href="https://www.smartsmith.com/services/web-development">Website Development</a> and <a href="https://www.smartsmith.com/services/graphic-design">Designing</a> by <a href="https://www.smartsmith.com/">SmartSmith Infotech</a>
    </div>
</footer>
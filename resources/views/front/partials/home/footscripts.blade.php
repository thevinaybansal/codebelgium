  {{-- JavaScript Files  --}}


  {{-- Javascript Compiles --}}
  {{-- <script src="{{ mix('js/manifest.js', 'assets/web') }}"></script> --}}
  {{-- <script src="{{ mix('js/vendor.js', 'assets/web') }}"></script> --}}
  <script src="{{ mix('js/app.js', 'assets/web') }}"></script>
  
  {{-- jquery --}}
  {{-- <script src="{{ asset(config('directory.assets-theme-js') . 'jquery.min.js') }}"></script> --}}

  {{-- QuickQuote Script --}}
  <script>
    $(document).ready(function(){
      $(".quickquote_side_button").click(function(){
        $(".quickquote_side_box").addClass("quickquote_side_boxopen");
      });
  
      $(".quickboxclose").click(function(){
        $(".quickquote_side_box").removeClass("quickquote_side_boxopen");
      });
    });
  </script>
  
  {{-- Inline Sticky Header --}}
  <script>$(window).scroll(function(){if($(this).scrollTop()>0){$('header').addClass('sticky');}else{$('header').removeClass('sticky');}});</script>

  {{-- Recaptcha --}}
  <script src="https://www.google.com/recaptcha/api.js?render={{ config('recaptcha.key') }}"></script>
  <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('{{ config('recaptcha.key') }}', {action: 'contact_form'}).then(function(token) {
           var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
        });
    });
  </script>

  {{-- To hide Flash in 3 seconds --}}
  <script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
  </script>


  <script type="text/javascript">
    $('.animate').scrolla({
      mobile: false,
      once: true
    });
  </script>
  
  {{-- Bootstrap Bundle --}}
  {{-- <script src="{{ asset(config('directory.assets-theme-js') . 'bootstrap.bundle.min.js') }}"></script> --}}
  {{-- FlexSlider --}}
  {{-- <script defer src="{{ asset(config('directory.assets-theme-js') . 'jquery.flexslider.js') }}"></script> --}}


  {{-- Flex Slider --}}
  <script type="text/javascript">
    $(function(){
      //SyntaxHighlighter.all();
    });
    $(window).on('load', function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>

  
  {{-- On Demand Page Scripts --}}
  @stack('js')
  
  {{-- On Demand Scripts --}}
  @stack('scripts')
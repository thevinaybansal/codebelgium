<!DOCTYPE html>
<html dir="ltr" lang="en-US">

  {{-- Your Stylesheets, Scripts & Title
  ============================================= --}}
  @include('front.partials.home.head')

  <body>
    
    {{-- Google Tag Manager --}}
    @include('googletagmanager::body')
    {{-- End Google Tag Manager --}}

    {{-- Quick Quote --}}
    {{-- <div class="quickquote_side">
      <div class="quickquote_side_button">
      <img src="{{ asset(config('directory.assets-theme-images') . 'quick-quote-side.png') }}"   alt="" />
      <img src="{{ asset(config('directory.assets-theme-images') . 'quick-quote-side-s.png') }}"   alt="" />
    </div>
      <div class="quickquote_side_box">  
        <i class="fa fa-times quickboxclose" aria-hidden="true"></i>
        <h3>Request Callback</h3>
        {{ Form::open(array('action' => 'FrontendController@contactSubmission')) }}
          {{ Form::text('name', null, array('placeholder' => 'Name') ) }}
          {{ Form::text('phone', null, array('placeholder' => 'Phone') ) }}
          {{ Form::email('email', null, array('placeholder' => 'E-mail') ) }}
          {{ Form::text('message', null, array('placeholder' => 'Message') ) }}
          {{ Form::submit('Submit') }}
        {{ Form::close() }}
      </div>
    </div> --}}
  
    @include('front.partials.home.header')
    {{-- Slider --}}
    @yield('slider')

    @include('front.partials.home.content')
    @include('front.partials.home.footer')
    @include('front.partials.home.copyrights')
    @include('front.partials.home.footscripts')
  </body>
</html>
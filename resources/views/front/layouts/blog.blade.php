<!DOCTYPE html>
<html dir="ltr" lang="en-US">

  {{-- Your Stylesheets, Scripts & Title
  ============================================= --}}
  @include('front.partials.home.head')

  <body>
    
    {{-- Google Tag Manager --}}
    @include('googletagmanager::body')
    {{-- End Google Tag Manager --}}


    @include('front.partials.blog.header')
    {{-- Slider --}}
    @yield('slider')

    @include('front.partials.blog.content')
    @include('front.partials.home.footer')
    @include('front.partials.home.copyrights')
    @include('front.partials.home.footscripts')
  </body>
</html>
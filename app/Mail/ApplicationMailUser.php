<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Application;

class ApplicationMailUser extends Mailable
{
    use Queueable, SerializesModels;


    public $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        //
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $course = $this->application->course;
        if($course = 'coding')
            return $this->from('amruta@codebelgium.com', 'Amruta from CodeBelgium')
                    ->subject("Application for Software Engineering Bootcamp at CodeBelgium")
                    ->view('emails.application_user_coding');
        else if($course = 'coding-remote')
            return $this->from('amruta@codebelgium.com', 'Amruta from CodeBelgium')
                    ->subject("Application for Remote Software Engineering Course at CodeBelgium")
                    ->view('emails.application_user_coding_remote');
        else
            dd("error");
    }
}

<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class ContactFormRequest extends FormRequest
{


    /**
     * The route to redirect to if validation fails.
     *
     * @var string
     */
    // protected $redirect  = $url->previous() . '#contact';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $this->redirect = $url->previous() . '#contact';
        return [
            //
            'name'                      =>  'required|min:4',
            'phone'                     =>  'required|min:9|regex:/^[0-9 +-]*$/|max:16',
            'email'                     =>  'required|email',
            'message'                   =>  'required|min:10',
        ];
        
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'                     => 'Please enter your name',
            'name.min'                          => 'Please enter your Full name',
            'phone.required'                    => 'Please enter your phone number',
            'phone.min'                         => 'Please enter your complete phone number',
            'phone.regex'                       => 'Please enter a valid Phone Number',
            'phone.max'                         => 'Please enter only your Phone Number',
            'email.required'                    => 'Please enter your email Address',
            'email.email'                       => 'Please enter your correct email Address',
            'message.required'                  => 'Please enter your message',
            'message.min'                       => 'Please enter your Complete message',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        flash('There was some problem with your contact form submission, Please fill and send again')->error();
        throw (new ValidationException($validator))
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl() . "#contact");
    }



}

const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


const rootPath = Mix.paths.root.bind(Mix.paths);

mix.setPublicPath(path.normalize('public/assets/web'));
mix.setResourceRoot('/assets/web/');
// mix.setResourceRoot('..');


mix.js('resources/js/app.js', 'js')
    .sass('resources/sass/app.scss', 'css');


// mix.copyDirectory('resources/theme/media/images', 'public/assets/theme/images');
// mix.copyDirectory('resources/theme/site-assets', 'public/assets/site');



// copy asset folder into laravel public folder
mix.copyDirectory('resources/assets/frontend/media/images', 'public/assets/web/theme/images');
// mix.copyDirectory('resources/assets/admin/', 'public/adminassets/theme');
mix.copyDirectory('resources/assets/site', 'public/assets/site');